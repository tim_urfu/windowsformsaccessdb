﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAccessDB
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            InitializeComponent();
        }

        private void button_Add_Click(object sender, EventArgs e)
        {
            FormAdd form = new FormAdd();
            form.Show();
        }

        private void button_SimpleDataGrid_Click(object sender, EventArgs e)
        {
            FormDataGridSimple form = new FormDataGridSimple();
            form.Show();
        }

        private void button_DataGridManual_Click(object sender, EventArgs e)
        {
            FormDataGridManual form = new FormDataGridManual();
            form.Show();
        }

        private void button_DataGridWithUpdate_Click(object sender, EventArgs e)
        {
            FormDataGridUpdate form = new FormDataGridUpdate();
            form.Show();
        }

        private void button_ComboBox_Click(object sender, EventArgs e)
        {
            FormComboBox form = new FormComboBox();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FormComboBoxManual form = new FormComboBoxManual();
            form.Show();
        }

        private void button_reportViewer_Click(object sender, EventArgs e)
        {
            FormReport form = new FormReport();
            form.Show();
        }

        private void button_chartForm_Click(object sender, EventArgs e)
        {
            FormChart form = new FormChart();
            form.Show();
        }
    }
}
