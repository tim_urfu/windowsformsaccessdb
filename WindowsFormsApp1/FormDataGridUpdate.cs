﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAccessDB
{
    public partial class FormDataGridUpdate : Form
    {
        public FormDataGridUpdate()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "universityDataSet.Groups". При необходимости она может быть перемещена или удалена.
            this.groupsTableAdapter.Fill(this.universityDataSet.Groups);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "universityDataSet.Students". При необходимости она может быть перемещена или удалена.
            this.studentsTableAdapter.Fill(this.universityDataSet.Students);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "universityDataSet.Students". При необходимости она может быть перемещена или удалена.
            this.studentsTableAdapter.Fill(this.universityDataSet.Students);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.studentsBindingSource.EndEdit();
            
            this.studentsTableAdapter.Update(this.universityDataSet.Students);
            this.studentsTableAdapter.Fill(this.universityDataSet.Students);

            MessageBox.Show("OK");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.studentsTableAdapter.Fill(this.universityDataSet.Students);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormDataGridManual form = new FormDataGridManual();
            form.Show();
        }

        private void button_example_3_Click(object sender, EventArgs e)
        {
            FormComboBoxManual form = new FormComboBoxManual();
            form.Show();
        }

        private void studentsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void studentsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            //this.studentsDataGridView.Rows[studentsDataGridView.CurrentCell.RowIndex].Cells["GroupId"].Value = 22;
        }

        private void button_filter_Click(object sender, EventArgs e)
        {
            string filterField = null;

            if (comboBox_filterWhere.SelectedIndex == 1) filterField = "Lastname";
            else if (comboBox_filterWhere.SelectedIndex == 2) filterField = "Firstname";
            else if (comboBox_filterWhere.SelectedIndex == 3) filterField = "Phone";

            if (filterField is null)
                studentsBindingSource1.Filter = null;
            else
            {
                studentsBindingSource1.Filter = string.Format("{0} LIKE '%{1}%'", filterField, textBox_filterStr.Text);
            }

            studentsDataGridView.Refresh();
        }
    }
}
