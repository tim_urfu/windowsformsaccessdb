﻿namespace WindowsFormsAccessDB
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Add = new System.Windows.Forms.Button();
            this.button_SimpleDataGrid = new System.Windows.Forms.Button();
            this.button_DataGridManual = new System.Windows.Forms.Button();
            this.button_DataGridWithUpdate = new System.Windows.Forms.Button();
            this.button_ComboBox = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_reportViewer = new System.Windows.Forms.Button();
            this.button_chartForm = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_Add
            // 
            this.button_Add.Location = new System.Drawing.Point(12, 69);
            this.button_Add.Name = "button_Add";
            this.button_Add.Size = new System.Drawing.Size(118, 51);
            this.button_Add.TabIndex = 0;
            this.button_Add.Text = "Добавление элемента через запрос";
            this.button_Add.UseVisualStyleBackColor = true;
            this.button_Add.Click += new System.EventHandler(this.button_Add_Click);
            // 
            // button_SimpleDataGrid
            // 
            this.button_SimpleDataGrid.Location = new System.Drawing.Point(12, 12);
            this.button_SimpleDataGrid.Name = "button_SimpleDataGrid";
            this.button_SimpleDataGrid.Size = new System.Drawing.Size(114, 51);
            this.button_SimpleDataGrid.TabIndex = 1;
            this.button_SimpleDataGrid.Text = "DataGrid без обновления данных";
            this.button_SimpleDataGrid.UseVisualStyleBackColor = true;
            this.button_SimpleDataGrid.Click += new System.EventHandler(this.button_SimpleDataGrid_Click);
            // 
            // button_DataGridManual
            // 
            this.button_DataGridManual.Location = new System.Drawing.Point(136, 69);
            this.button_DataGridManual.Name = "button_DataGridManual";
            this.button_DataGridManual.Size = new System.Drawing.Size(118, 51);
            this.button_DataGridManual.TabIndex = 2;
            this.button_DataGridManual.Text = "Ручное заполнение DataGrid";
            this.button_DataGridManual.UseVisualStyleBackColor = true;
            this.button_DataGridManual.Click += new System.EventHandler(this.button_DataGridManual_Click);
            // 
            // button_DataGridWithUpdate
            // 
            this.button_DataGridWithUpdate.Location = new System.Drawing.Point(132, 12);
            this.button_DataGridWithUpdate.Name = "button_DataGridWithUpdate";
            this.button_DataGridWithUpdate.Size = new System.Drawing.Size(122, 51);
            this.button_DataGridWithUpdate.TabIndex = 3;
            this.button_DataGridWithUpdate.Text = "DataGrid с обновлением данных";
            this.button_DataGridWithUpdate.UseVisualStyleBackColor = true;
            this.button_DataGridWithUpdate.Click += new System.EventHandler(this.button_DataGridWithUpdate_Click);
            // 
            // button_ComboBox
            // 
            this.button_ComboBox.Location = new System.Drawing.Point(260, 12);
            this.button_ComboBox.Name = "button_ComboBox";
            this.button_ComboBox.Size = new System.Drawing.Size(106, 51);
            this.button_ComboBox.TabIndex = 4;
            this.button_ComboBox.Text = "ComboBox";
            this.button_ComboBox.UseVisualStyleBackColor = true;
            this.button_ComboBox.Click += new System.EventHandler(this.button_ComboBox_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(260, 69);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 51);
            this.button1.TabIndex = 5;
            this.button1.Text = "ComboBox с ручным заполнением";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_reportViewer
            // 
            this.button_reportViewer.Location = new System.Drawing.Point(136, 126);
            this.button_reportViewer.Name = "button_reportViewer";
            this.button_reportViewer.Size = new System.Drawing.Size(118, 51);
            this.button_reportViewer.TabIndex = 6;
            this.button_reportViewer.Text = "Отчет ReportViewer";
            this.button_reportViewer.UseVisualStyleBackColor = true;
            this.button_reportViewer.Click += new System.EventHandler(this.button_reportViewer_Click);
            // 
            // button_chartForm
            // 
            this.button_chartForm.Location = new System.Drawing.Point(12, 126);
            this.button_chartForm.Name = "button_chartForm";
            this.button_chartForm.Size = new System.Drawing.Size(118, 51);
            this.button_chartForm.TabIndex = 7;
            this.button_chartForm.Text = "Работа с элементом Chart";
            this.button_chartForm.UseVisualStyleBackColor = true;
            this.button_chartForm.Click += new System.EventHandler(this.button_chartForm_Click);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 214);
            this.Controls.Add(this.button_chartForm);
            this.Controls.Add(this.button_reportViewer);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button_ComboBox);
            this.Controls.Add(this.button_DataGridWithUpdate);
            this.Controls.Add(this.button_DataGridManual);
            this.Controls.Add(this.button_SimpleDataGrid);
            this.Controls.Add(this.button_Add);
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Работа с Access DB в C#";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_Add;
        private System.Windows.Forms.Button button_SimpleDataGrid;
        private System.Windows.Forms.Button button_DataGridManual;
        private System.Windows.Forms.Button button_DataGridWithUpdate;
        private System.Windows.Forms.Button button_ComboBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_reportViewer;
        private System.Windows.Forms.Button button_chartForm;
    }
}