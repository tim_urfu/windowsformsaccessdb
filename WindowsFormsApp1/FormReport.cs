﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAccessDB
{
    public partial class FormReport : Form
    {
        public FormReport()
        {
            InitializeComponent();
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            // Заполнение таблицы
            UniversityDataSet uds = new UniversityDataSet();
            UniversityDataSet.StudentsDataTable bs = new UniversityDataSet.StudentsDataTable();

            UniversityDataSetTableAdapters.StudentsTableAdapter sta = new UniversityDataSetTableAdapters.StudentsTableAdapter();
            sta.Fill(bs);

            // Передача источника в отчет
            ReportDataSource rds = new ReportDataSource("StudentsDataTable", (DataTable)bs);
            reportViewer2.LocalReport.DataSources.Add(rds);

            // Передача параметра в отчет
            ReportParameter param1 = new ReportParameter("ReportParameter1", "Отчет по студентам");
            reportViewer2.LocalReport.SetParameters(param1);

            reportViewer2.RefreshReport();
        }
    }
}
