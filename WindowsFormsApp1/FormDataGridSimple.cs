﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsAccessDB
{
    public partial class FormDataGridSimple : Form
    {
        OleDbConnection conn;

        OleDbCommand cmd = new OleDbCommand();

        public FormDataGridSimple()
        {
            InitializeComponent();
        }

        private void FormSimpleDataGrid_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "universityDataSet.StudentsWithGroup". При необходимости она может быть перемещена или удалена.
            this.studentsWithGroupTableAdapter.Fill(this.universityDataSet.StudentsWithGroup);
        }

        private void button_close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_filter_Click(object sender, EventArgs e)
        {
            string filterField = null;

            if (comboBox_filterWhere.SelectedIndex == 1) filterField = "Lastname";
            else if (comboBox_filterWhere.SelectedIndex == 2) filterField = "Firstname";
            else if (comboBox_filterWhere.SelectedIndex == 3) filterField = "Phone";

            if (filterField is null)
                studentsWithGroupBindingSource.Filter = null;
            else
            {
                studentsWithGroupBindingSource.Filter = string.Format("{0} LIKE '%{1}%'", filterField, textBox_filterStr.Text);
            }
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.studentsWithGroupBindingSource.EndEdit();

            //this.studentsWithGroupTableAdapter.Update(this.universityDataSet.Students);
            //this.studentsWithGroupTableAdapter.Fill(this.universityDataSet.Students);

            MessageBox.Show("OK");
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.ColumnIndex == 9)
            {
                DataGridViewTextBoxCell cell = (DataGridViewTextBoxCell)dataGridView1.Rows[e.RowIndex].Cells[0];

                DeleteStudentById(cell.Value.ToString());

                this.studentsWithGroupTableAdapter.Fill(this.universityDataSet.StudentsWithGroup);
                dataGridView1.Refresh();
            }
        }

        private void DeleteStudentById(string StudentId)
        {
            // Если не открывать соединение каждый раз заново, то данные в dataGridView обновляются с задержкой
            conn = new OleDbConnection(WindowsFormsAccessDB.Properties.Settings.Default.UniversityConnectionString);
            conn.Open();

            cmd.Connection = conn;
            cmd.CommandText = "DELETE FROM Students WHERE StudentId = " + StudentId;
            cmd.ExecuteNonQuery();
            conn.Close();
        }
    }
}
