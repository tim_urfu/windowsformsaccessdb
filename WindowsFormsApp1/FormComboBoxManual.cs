﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsAccessDB;

namespace WindowsFormsAccessDB
{
    public partial class FormComboBoxManual : Form
    {
        OleDbConnection conn;

        OleDbCommand cmd = new OleDbCommand();

        public FormComboBoxManual()
        {
            InitializeComponent();

            conn = new OleDbConnection(WindowsFormsAccessDB.Properties.Settings.Default.UniversityConnectionString);
            conn.Open();
        }

        private void Form4_Load(object sender, EventArgs e)
        {
            cmd.Connection = conn;
            cmd.CommandText = "SELECT Lastname, Firstname FROM Students";
            
            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read()) // построчно считываем данные
            {
                comboBox1.Items.Add(reader.GetValue(0) + " " + reader.GetValue(1));
            }

            comboBox1.SelectedIndex = 0;
        }
    }
}
