﻿using System;
using System.Data.OleDb;
using System.Windows.Forms;

namespace WindowsFormsAccessDB
{
    public partial class FormAdd : Form
    {
        OleDbConnection conn;
        OleDbCommand cmd;

        public FormAdd()
        {
            InitializeComponent();

            conn = new OleDbConnection(WindowsFormsAccessDB.Properties.Settings.Default.UniversityConnectionString);
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            conn.Open();

            cmd = new OleDbCommand("INSERT INTO Students (Firstname, Lastname, Phone, Email, Birthday, GroupId) values (@Firstname, @Lastname, @Phone, @Email, @Birthday, 1)", conn);

            cmd.Parameters.AddWithValue("@Firstname", this.textBox_firstname.Text);
            cmd.Parameters.AddWithValue("@Lastname", textBox_lastname.Text);
            cmd.Parameters.AddWithValue("@Phone", textBox_phone.Text);
            cmd.Parameters.AddWithValue("@Email", textBox_email.Text);
            cmd.Parameters.AddWithValue("@Birthday", dateTimePicker_Birthday.Value.Date);

            cmd.ExecuteNonQuery();

            this.Close();
        }

        private void FormAdd_Load(object sender, EventArgs e)
        {

        }
    }
}
