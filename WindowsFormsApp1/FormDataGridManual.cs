﻿using System;
using System.Windows.Forms;
using System.Data.OleDb;
using WindowsFormsAccessDB;

namespace WindowsFormsAccessDB
{
    public partial class FormDataGridManual : Form
    {
        UniversityDataSet _universityDataSet = new UniversityDataSet();

        OleDbConnection conn;

        public FormDataGridManual()
        {
            InitializeComponent();

            conn = new OleDbConnection(WindowsFormsAccessDB.Properties.Settings.Default.UniversityConnectionString);
            conn.Open();
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            OleDbDataAdapter adapter = new OleDbDataAdapter();
            adapter.SelectCommand = new OleDbCommand("SELECT * FROM Students", conn);

            adapter.Fill(_universityDataSet.Students);

            dataGridView1.DataSource = _universityDataSet.Students;
        }
    }
}
