﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsAccessDB.Models;

namespace WindowsFormsAccessDB
{
    public partial class FormChart : Form
    {
        OleDbConnection conn;

        OleDbCommand cmd = new OleDbCommand();

        public FormChart()
        {
            InitializeComponent();
        }

        private void FormChart_Load(object sender, EventArgs e)
        {
            ChartUpdate();
        }

        private void ChartUpdate()
        {
            List<ChartGroup> groupData = new List<ChartGroup>();

            conn = new OleDbConnection(Properties.Settings.Default.UniversityConnectionString);
            conn.Open();

            #region -- Get groups

            var query = string.IsNullOrEmpty(textBox1.Text) ? "1 = 1" : "GroupName LIKE '%" + textBox1.Text  + "%'";

            cmd = new OleDbCommand("SELECT GroupId, GroupName FROM Groups WHERE " + query, conn);

            OleDbDataReader reader = cmd.ExecuteReader();

            while (reader.Read()) // построчно считываем данные
            {
                groupData.Add(new ChartGroup
                {
                    GroupId = Convert.ToInt32(reader.GetValue(0)),
                    Name = reader.GetValue(1).ToString()
                });
            }

            #endregion

            #region -- Get data

            cmd = new OleDbCommand("SELECT GroupId, COUNT(*) AS [count] FROM Students GROUP BY GroupId", conn);

            reader = cmd.ExecuteReader();

            while (reader.Read()) // построчно считываем данные
            {
                var groupId = Convert.ToInt32(reader.GetValue(0));
                var groupRow = groupData.FirstOrDefault(p => p.GroupId == groupId);

                if(groupRow != null)
                {
                    groupRow.Count = Convert.ToInt32(reader.GetValue(1));
                }
            }

            #endregion

            conn.Close();

            #region -- Chart update

            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();

            foreach (var point in groupData)
            {
                chart1.Series[0].Points.AddXY(point.Name, point.Count);

                if(point.Count > 0)
                {
                    chart2.Series[0].Points.AddXY(point.Name, point.Count);
                }
            }

            #endregion
        }

        private void button_Apply_Click(object sender, EventArgs e)
        {
            ChartUpdate();
        }
    }
}
