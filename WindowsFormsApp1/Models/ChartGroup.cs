﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsAccessDB.Models
{
    public class ChartGroup
    {
        public int GroupId { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }
    }
}
